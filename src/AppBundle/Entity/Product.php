<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 08/10/2018
 * Time: 17:58
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class Product
 * @package AppBundle\Entity
 * @ORM\Entity(repositoryClass="AppBundle\Entity\ProductRepository")
 * @ORM\Table()
 * @Serializer\ExclusionPolicy("all")
 * @Serializer\VirtualProperty("md5", exp="object.getMd5()" )
 */
class Product
{

    /**
     * @var integer
     * @ORM\Id()
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Expose()
     */
    protected $id;

    /**
     * @var boolean
     * @ORM\Column(name="active", type="boolean", nullable=false)
     *
     *
     */
    protected $active;

    /**
     * @var string
     * @ORM\Column(name="name", type="string", nullable=false, length=255)
     * @Serializer\Expose()
     */
    protected $name;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Serializer\Expose()
     */
    protected $url;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     * @Serializer\Expose()
     */
    protected $description;

    /**
     * @var Brand|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Brand")
     * @ORM\JoinColumn(nullable=true, referencedColumnName="id", name="brand_id")
     * @Serializer\Expose()
     */
    protected $brand;


    /**
     * @var ArrayCollection|[]
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Category", mappedBy="products", cascade={"persist","remove"})
     * @Serializer\Expose()
     *
     */
    protected $categories;


    /**
     * Constructor
     */
    public function __construct()
    {
        $this->categories = new \Doctrine\Common\Collections\ArrayCollection();
        $this->active = true;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set active.
     *
     * @param bool $active
     *
     * @return Product
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active.
     *
     * @return bool
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return Product
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set url.
     *
     * @param string|null $url
     *
     * @return Product
     */
    public function setUrl($url = null)
    {
        $this->url = $url;

        return $this;
    }

    /**
     * Get url.
     *
     * @return string|null
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * Set description.
     *
     * @param string|null $description
     *
     * @return Product
     */
    public function setDescription($description = null)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description.
     *
     * @return string|null
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set brand.
     *
     * @param \AppBundle\Entity\Brand|null $brand
     *
     * @return Product
     */
    public function setBrand(\AppBundle\Entity\Brand $brand = null)
    {
        $this->brand = $brand;


        return $this;
    }

    /**
     * Get brand.
     *
     * @return \AppBundle\Entity\Brand|null
     */
    public function getBrand()
    {
        return $this->brand;
    }


    /**
     * Add category.
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return Product
     */
    public function addCategory(\AppBundle\Entity\Category $category)
    {
        $category->addProduct($this);
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove category.
     *
     * @param \AppBundle\Entity\Category $category
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeCategory(\AppBundle\Entity\Category $category)
    {
        return $this->categories->removeElement($category);
    }

    /**
     * Get categories.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCategories()
    {
        return $this->categories;
    }

    /**
     * @return string
     */
    public function getMd5(): string
    {
        return md5($this->getId());
    }


    public function __toString()
    {
        return $this->getName();
    }
}
