<?php

namespace AppBundle\Controller;

use AppBundle\Exception\InvalidFormException;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

/**
 * Class CategoryController
 * @package AppBundle\Controller
 *
 */
class CategoryController extends BaseApiController
{
    /**
     * @return \AppBundle\Manager\CategoryManager|object
     */
    protected function getManager()
    {
        return $this->get('app.manager.category_manager');
    }

    /**
     * @param Request $request
     * @Rest\Get("/categories/{id}", requirements={"id":"\d+"}, name="get_single_category")
     * @ApiDoc(
     *     section="Category",
     *     description="Return a single Category",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      404="Returned when not found",
     *      500="Returned when something get wrong"
     *     })
     * @return mixed|null
     */
    public function getAction(Request $request)
    {
        $id = $request->get('id');
        $category = $this->getManager()->get($id);
        if(!$category){
            $this->throwCategoryNotFoundException();
        }

        return $category;


    }

    /**
     * @param Request $request
     * @Rest\Get("/categories", name="get_collection_categories")
     * @ApiDoc(
     *     section="Category",
     *     description="Return a list of Category",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      500="Returned when something get wrong"
     *     },
     *     filters={
     *          {"name"="page", "dataType"="integer", "default"="1"},
     *          {"name"="limit", "dataType"="integer", "default"="10"},
     *     })
     * @return \Knp\Component\Pager\Pagination\PaginationInterface
     */
    public function cgetAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 10);

        return $this->getManager()->all($page, $limit);
    }

    /**
     * @param Request $request
     * @Rest\Post("/categories", name="post_a_new_category")
     * @return mixed|JsonResponse
     * @ApiDoc(
     *     section="Category",
     *     description="Post a new category",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when form data are invalid",
     *      500="Returned when something else get wrong"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="category name"}
     *  },
     *     output="AppBundle\Entity\Category"
     * )
     */
    public function postAction(Request $request)
    {
        try{
            $parameters = $request->request->all();
            $options = ['method' => $request->getMethod()];

            return $this->getManager()->post($parameters, $options);
        }catch (InvalidFormException $exception){
           return $this->returnSerializedFormError($exception);
        }
    }

    /**
     * @param Request $request
     * @Rest\Patch("/categories/{id}", requirements={"id": "\d+"}, name="patch_category")
     * @return mixed|JsonResponse
     *
     * @ApiDoc(
     *     section="Category",
     *     description="Update an existing category",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when form data are invalid",
     *      404="Returned when category not found",
     *      500="Returned when something else get wrong"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="category name"}
     *  },
     *     output="AppBundle\Entity\Category"
     * )
     *
     */
    public function patchAction(Request $request)
    {
        $id = $request->get('id');

        $category = $this->getManager()->get($id);

        if(!$category){
            $this->throwCategoryNotFoundException();
        }

        try{

            $parameters = $request->request->all();
            $options = ['method' => $request->getMethod()];

            return $this->getManager()->patch($category, $parameters, $options);
        }catch (InvalidFormException $exception){
            return $this->returnSerializedFormError($exception);
        }


    }

    /**
     * @param Request $request
     * @Rest\Delete("/categories/{id}", requirements={"id": "\d+"}, name="delete_category_route")
     * @return JsonResponse
     * @ApiDoc(
     *     section="Category",
     *     description="Delete an existing Category",
     *     resource=true,
     *     statusCodes={
     *      204="Returned when successful",
     *      404="Returned when category is not found",
     *      500="Returned when something else get wrong"
     *     },
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $category = $this->getManager()->get($id);

        if(!$category){
            $this->throwCategoryNotFoundException();
        }

        $this->getManager()->delete($category);
        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);

    }

    private function throwCategoryNotFoundException()
    {
        throw new NotFoundHttpException('Category not found');
    }

}
