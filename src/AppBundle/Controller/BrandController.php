<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 10/8/18
 * Time: 11:44 PM
 */

namespace AppBundle\Controller;


use AppBundle\Exception\InvalidFormException;
use AppBundle\Manager\BaseManager;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class BrandController extends BaseApiController
{

    /**
     * @return BaseManager
     */
    protected function getManager()
    {
        return $this->get('app.manager.brand_manager');
    }

    /**
     * @param Request $request
     * @return mixed
     * Return single object
     * @Rest\Get("/brands/{id}", requirements={"id":"\d+"}, name="get_single_brand")
     * @ApiDoc(
     *     section="Brand",
     *     description="Return a single brand",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      404="Returned when not found",
     *      500="Returned when something get wrong"
     *     })
     */
    public function getAction(Request $request)
    {
        $brandId = $request->get('id');
        $brand = $this->getManager()->get($brandId);
        if(!$brand){
            throw new NotFoundHttpException('Brand not found');
        }

        return $brand;
    }

    /**
     * @param Request $request
     * @return mixed
     * Return collection
     * @Rest\Get("/brands", name="get_collection_brand")
     * @ApiDoc(
     *     section="Brand",
     *     description="Return a list of brand",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      500="Returned when something get wrong"
     *     },
     *     filters={
     *          {"name"="page", "dataType"="integer", "default"="1"},
     *          {"name"="limit", "dataType"="integer", "default"="10"},
     *     })
     */
    public function cgetAction(Request $request)
    {
        $page = $request->get('page', 1);
        $limit = $request->get('limit', 10);

        return $this->getManager()->all($page, $limit);


    }

    /**
     * @param Request $request
     * @return mixed
     * @Rest\Post("/brands", name="post_a_new_brand_route")
     * @ApiDoc(
     *     section="Brand",
     *     description="Post a new brand",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when form data are invalid",
     *      500="Returned when something else get wrong"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="brand name"}
     *  },
     *     output="AppBundle\Entity\Brand"
     * )
     */
    public function postAction(Request $request)
    {
        try {
            $parameters = $request->request->all();
            $options = ['method' => $request->getMethod()];

            return $this->getManager()->post($parameters, $options);
        } catch (InvalidFormException $exception) {
            return $this->returnSerializedFormError($exception);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     * @Rest\Patch("/brands/{id}", requirements={"id":"\d+"}, name="patch_brand_route")
     * @ApiDoc(
     *     section="Brand",
     *     description="Update an existing brand",
     *     resource=true,
     *     statusCodes={
     *      200="Returned when successful",
     *      400="Returned when form data are invalid",
     *      404="Returned when brand not found",
     *      500="Returned when something else get wrong"
     *     },
     *  parameters={
     *      {"name"="name", "dataType"="string", "required"=true, "description"="brand name"}
     *  },
     *     output="AppBundle\Entity\Category"
     * )
     */
    public function patchAction(Request $request)
    {
        $id = $request->get('id');

        $brand = $this->getManager()->get($id);

        if (!$brand) {
            throw new NotFoundHttpException('Brand not found');
        }

        try {

            $parameters = $request->request->all();
            $options = ['method' => $request->getMethod()];

            return $this->getManager()->patch($brand, $parameters, $options);
        } catch (InvalidFormException $exception) {
            return $this->returnSerializedFormError($exception);
        }
    }

    /**
     * @param Request $request
     * @return mixed
     * @Rest\Delete("/brands/{id}", name="delete_brand_route", requirements={"id":"\d+"})
     * @ApiDoc(
     *     section="Brand",
     *     description="Delete an existing brand",
     *     resource=true,
     *     statusCodes={
     *      204="Returned when successful",
     *      404="Returned when category is not found",
     *      500="Returned when something else get wrong"
     *     },
     * )
     */
    public function deleteAction(Request $request)
    {
        $id = $request->get('id');
        $brand = $this->getManager()->get($id);

        if (!$brand) {
            throw new NotFoundHttpException('Brand not found');
        }

        $this->getManager()->delete($brand);

        return $this->json(null, JsonResponse::HTTP_NO_CONTENT);
    }

}