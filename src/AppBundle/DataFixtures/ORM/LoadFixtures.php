<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Brand;
use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;

class LoadFixtures implements FixtureInterface {

    /**
     * Load data fixtures with the passed EntityManager
     *
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $brands=['HERMES', 'PRADA','CHANEL','RALPHLAUREN','BURBERRY','HOUSEOFVERSACE','FENDI','ARMANI'];

        $brandEntities = [];
        foreach ($brands as $brand) {
            $br = (new Brand())->setName($brand);
            $brandEntities[] = $br;
            $manager->persist($br);
        }



        $categories= ['Pantalon', 'Chemise', 'Chaussure de ville', 'Accessoires'];
        $categoryEntities = [];
        foreach ($categories as $category) {

            $cat = (new Category())->setName($category);
            $categoryEntities [] = $cat;
            $manager->persist($cat);
        }


        $products = range(0, random_int(87, 103));
        foreach ($products as $product) {
            $myProduct = new Product();
            $myProduct->setName("Product #". $product);
            $myProduct->setBrand($brandEntities[random_int(0, \count($brandEntities) - 1)]);
            $myProduct->addCategory($categoryEntities[random_int(0, \count($categoryEntities) - 1)]);
            $myProduct->setDescription("Product description : Ubi est flavum lumen? _". $product );
            $myProduct->setActive(true);
            $myProduct->setUrl("/api/products/".$product);

            $manager->persist($myProduct);

        }

        $manager->flush();
    }
}