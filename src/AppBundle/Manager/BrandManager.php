<?php
/**
 * Created by PhpStorm.
 * User: medoune
 * Date: 10/8/18
 * Time: 11:31 PM
 */

namespace AppBundle\Manager;


use AppBundle\Entity\Brand;
use AppBundle\Form\Handler\BaseFormHandler;
use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\PaginatorInterface;

class BrandManager extends BaseManager
{

    protected $formHandler;

    public function __construct(EntityRepository $repository, PaginatorInterface $paginator, BaseFormHandler $formHandler)
    {
        parent::__construct($repository, $paginator);
        $this->formHandler = $formHandler;
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    public function get($id)
    {
        return $this->repository->find($id);
    }

    public function all($page = 0, $limit = 10)
    {
        $data = $this->repository->all();
        return $this->paginator->paginate($data, $page, $limit);
    }

    /**
     * @param array $parameters
     * @param array $options
     * @return mixed
     */
    public function post(array $parameters, array $options)
    {
        return $this->formHandler->processForm(new Brand(), $parameters, $options['method']);
    }

    /**
     * @param $resource
     * @param array $parameters
     * @param array $options
     * @return mixed
     */
    public function put($resource, array $parameters, array $options)
    {
        return $this->formHandler->processForm($resource, $parameters, $options['method']);
    }

    /**
     * @param $resource
     * @param array $parameters
     * @param array $options
     * @return mixed
     */
    public function patch($resource, array $parameters, array $options)
    {
        return $this->put($resource, $parameters, $options);
    }

    /**
     * @param $resource
     * @return mixed
     */
    public function delete($resource)
    {
        return $this->formHandler->delete($resource);
    }
}