<?php
/**
 * Created by PhpStorm.
 * User: Qualshore
 * Date: 08/10/2018
 * Time: 18:31
 */

namespace AppBundle\Manager;


use Doctrine\ORM\EntityRepository;
use Knp\Component\Pager\PaginatorInterface;

abstract class BaseManager
{
    protected $repository;
    protected $paginator;

    public function __construct(EntityRepository $repository, PaginatorInterface $paginator)
    {
        $this->repository = $repository;
        $this->paginator = $paginator;
    }

    /**
     * @param int $id
     * @return mixed|null
     */
    abstract public function get($id);

    abstract public function all($page=0, $limit=10);

    /**
     * @param array $parameters
     * @param array $options
     * @return mixed
     */
    abstract public function post(array $parameters, array $options);

    /**
     * @param $resource
     * @param array $parameters
     * @param array $options
     * @return mixed
     */
    abstract public function put($resource, array $parameters, array $options);

    /**
     * @param $resource
     * @param array $parameters
     * @param array $options
     * @return mixed
     */
    abstract public function patch($resource, array $parameters, array $options);


    /**
     * @param $resource
     * @return mixed
     */
    abstract public function delete($resource);

}
